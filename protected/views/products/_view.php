<div class="post">
	<div class="title">
		<?php echo CHtml::link(CHtml::encode($data->name), $data->url); ?>
	</div>
	<div class="author">
		posted on <?php echo date('F j, Y',strtotime($data->createdTime)); ?>
	</div>
	<div class="content">
		<?php
			$this->beginWidget('CMarkdown', array('purifyOutput'=>true));
			echo $data->name;
			$this->endWidget();
		?>
	</div>
	<div class="nav">
		<?php echo CHtml::image('images/'.$data->image);?>
	</div>
</div>
