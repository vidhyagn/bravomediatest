<?php

class ProductsController extends Controller
{
	public $layout='column2';
	public $param = 'products';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */

	

	/**
	 * Lists all Products.
	 */
	public function actionIndex()
	{
		/******** Fetching products from the products table *********/

		$dataProvider=new CActiveDataProvider('Products', array(
			'pagination'=>false
		));

		/******** Rendering the data to view page ********/
		$this->render('index',array(
			'dataProvider'=>$dataProvider
		));
	}

	
}
